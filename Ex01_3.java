/*
 * Ex01_3.java
 *
 * Copyright(c) 2015 All Rights Reserved by Shinya Kinoshita
 */

/**
 * コマンドライン引数からドリンクのサイズ(L, M, S)を入力させ、次のような場合に
 * 応じたメッセージを出力する。
 *
 * <p>
 * コマンドライン引数にL, M, S以外の文字が入力された場合は、「L, M, Sのいずれかを入力
 * してください」と出力する。
 *
 * <p>
 * なお、小文字(l, m, s)が入力された場合は大文字に変換した上で処理を行う。
 *
 * <p>
 * 本クラスはif文を使って作成されている。<br>
 * また、文字列の比較を使って作成されている。
 *
 * @author Shinya Kinoshita
 * @version 1.0
 * @since JDK 6.0
 */
public class Ex01_3 {

  /**
   * プライベートコンストラクタ。
   */
  private Ex01_3() {
  }

  /**
   * プログラムを起動する。
   *
   * @param args コマンドライン引数 {@code args[0]}:ドリンクのサイズ
   * @throws ArrayIndexOutOfBoundsException {@code args.length == 0} の場合
   */
  public static void main(String[] args) {
    // 変数の宣言及び初期化をする。
    final String DRINK_SIZE_L = "L";  // ドリンク - Lサイズ
    final String DRINK_SIZE_M = "M";  // ドリンク - Mサイズ
    final String DRINK_SIZE_S = "S";  // ドリンク - Sサイズ
    String drinkSize = "";            // ドリンクのサイズ

    // ドリンクのサイズを取得する。
    drinkSize = args[0].toUpperCase();

    // ドリンクのサイズを出力する。
    if (drinkSize.equals(DRINK_SIZE_L)) {
      // ドリンク - Lサイズの場合
      System.out.println("￥280(300cc)です");
    } else if (drinkSize.equals(DRINK_SIZE_M)) {
        // ドリンク - Mサイズの場合
        System.out.println("￥230(200cc)です");
    } else if (drinkSize.equals(DRINK_SIZE_S)) {
        // ドリンク - Sサイズの場合
        System.out.println("￥180(150cc)です");
    } else {
      // 上記以外の場合
      System.out.println("L, M, Sのいずれかを入力してください");
    }
  }
}
