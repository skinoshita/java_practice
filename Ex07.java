/*
 * Ex07.java
 *
 * Copyright(c) 2015 All Rights Reserved by Shinya Kinoshita
 */

/**
 * コマンドライン引数から２個以上の点数を入力し、合計点と平均点と平均点以上の人数を出力する。
 *
 * <p>
 * また、得点の高い順に順位と得点を出力する。
 *
 * @author Shinya Kinoshita
 * @version 1.0
 * @since JDK 6.0
 */
public class Ex07 {

  /**
   * プライベートコンストラクタ。
   */
  private Ex07() {
  }

  /**
   * プログラムを起動する。
   *
   * @param args コマンドライン引数
   * @throws NumberFormatException コマンドライン引数に数字以外が入力された場合
   */
  public static void main(String[] args) {
    // 変数の宣言と初期化をする。
    int pointCount = args.length;               // 得点の数
    int[] points = new int[pointCount];         // 点数
    int[] sortedPoints = new int[pointCount];   // 得点の高い順に並べた点数
    int total = 0;                              // 合計点
    float average = 0.0f;                       // 平均点
    int overAveragePersonCount = 0;             // 平均点以上の人数
    int i = 0;                                  // ループ用インデックス
    int j = 0;                                  // ループ用インデックス
    int temp = 0;                               // 一時保存用

    // コマンドライン引数から点数を取得する。
    for (; i < pointCount; i++) {
      temp = Integer.parseInt(args[i]);
      points[i] = temp;
      sortedPoints[i] = temp;
    }

    // 合計点を計算する。
    for (i = 0; i < pointCount; i++) {
      total += points[i];
    }

    // 平均点を計算する。
    average = (float) total / pointCount;

    // 平均点以上の人数を計算する。
    for (i = 0; i < pointCount; i++) {
      if (average <= points[i]) {
        overAveragePersonCount++;
      }
    }

    // 得点の高い順に並び替える。
    for (i = 0; i < pointCount; i++) {
      for (j = 0; j < pointCount - 1 - i; j++) {
        if (sortedPoints[j] < sortedPoints[j + 1]) {
          temp = sortedPoints[j];
          sortedPoints[j] = sortedPoints[j + 1];
          sortedPoints[j + 1] = temp;
        }
      }
    }

    // 合計点を出力する。
    System.out.println("合計点は" + total + "です");

    // 平均点を出力する。
    System.out.println("平均点は" + average + "です");

    // 平均点以上の人数を表示する。
    System.out.println("平均点以上は" + overAveragePersonCount + "人です");

    // 得点の高い順に順位と得点を出力する。
    for (i = 0; i < pointCount; i++) {
      System.out.println((i + 1) + "位の人の点数は" + sortedPoints[i] + "です。");
    }
  }
}
