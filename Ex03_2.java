/*
 * Ex03_2.java
 *
 * Copyright(c) 2015 All Rights Reserved by Shinya Kinoshita
 */

 /**
  * コマンドライン引数からいずれかの整数を入力させ、以下のように入力された数値の
  * 九九を表示する。
  *
  * <p>
  * 本クラスはwhile文を使って作成されている。
  *
  * @author Shinya Kinoshita
  * @version 1.0
  * @since JDK 6.0
  */
public class Ex03_2 {

  /**
   * プライベートコンストラクタ。
   */
  private Ex03_2() {
  }

  /**
   * プログラムを起動する。
   *
   * @param args コマンドライン引数 {@code args[0]} : 表示したい九九の段の数値
   * @throws ArrayIndexOutOfBoundsException {@code args.length == 0} の場合
   * @throws NumberFormatException {@code args[0]} が数字以外の場合
   */
  public static void main(String[] args) {
    // 変数の宣言及び初期化をする。
    int targetNumber = 0;                   // 表示したい九九の段の数値
    int multiplicationTableIndex = 1;       // 表示したい九九の段の位置
    String multiplicationTableLine = "";    // 九九の段

    // コマンドライン引数から表示したい九九の段の数値を取得する。
    targetNumber = Integer.parseInt(args[0]);
    if (!(targetNumber >= 1 && targetNumber <= 9)) {
      // 1〜9以外の文字が入力された場合はエラーメッセージを表示してプログラムを終了する。
      System.out.println("必ず1〜9までの整数を入力してください");
      System.exit(0);
    }

    // 九九の段を取得する。
    while (multiplicationTableIndex <= 9) {
      multiplicationTableLine +=
          (targetNumber * multiplicationTableIndex) + " ";
      multiplicationTableIndex++;
    }

    // 九九の段を表示する。
    System.out.println(multiplicationTableLine);
  }
}
