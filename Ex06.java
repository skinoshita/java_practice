/*
 * Ex06.java
 *
 * Copyright(c) 2015 All Rights Reserved by Shinya Kinoshita
 */

/**
 * コマンドライン引数から西暦年を入力し、うるう年を判定する。
 *
 * @author Shinya Kinoshita
 * @version 1.0
 * @since JDK 6.0
 */
public class Ex06 {

  /**
   * プログラムを起動する。
   *
   * @param args コマンドライン引数 {@code args[0]} : 西暦年
   * @throws ArrayIndexOutOfBoundsException {@code args.length == 0} の場合
   * @throws NumberFormatException {@code args[0]} が数字以外の場合
   */
  public static void main(String[] args) {
    // 変数の宣言及び初期化をする。
    int year = 0;     // 西暦年

    // コマンドライン引数から西暦年を取得する。
    year = Integer.parseInt(args[0]);

    // うるう年を判定する。
    if (year % 100 == 0 && year % 400 == 0) {
      // 100で割り切れ、400でも割り切れる場合はうるう年とする。
      System.out.println(year + "年はうるう年です");
    } else if (year % 100 != 0 && year % 4 == 0) {
      // 100で割り切れず、4で割り切れる場合はうるう年とする。
      System.out.println(year + "年はうるう年です");
    } else {
      // 上記以外の場合はうるう年としない。
      System.out.println(year + "年はうるう年ではありません");
    }
  }
}