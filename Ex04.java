/*
 * Ex04.java
 *
 * Copyright(c) 2015 All Rights Reserved by Shinya Kinoshita
 */

/**
 * コマンドライン引数から２個以上の点数を入力し、合計点と平均点と平均点以上の人数を出力する。
 *
 * @author Shinya Kinoshita
 * @version 1.0
 * @since JDK 6.0
 */
public class Ex04 {

  /**
   * プライベートコンストラクタ。
   */
  private Ex04() {
  }

  /**
   * プログラムを起動する。
   *
   * @param args コマンドライン引数
   * @throws NumberFormatException コマンドライン引数に数字以外が入力された場合
   */
  public static void main(String[] args) {
    // 変数の宣言と初期化をする。
    int pointCount = args.length;         // 得点の数
    int[] points = new int[pointCount];   // 点数
    int total = 0;                        // 合計点
    float average = 0.0f;                 // 平均点
    int overAveragePersonCount = 0;       // 平均点以上の人数
    int i = 0;                            // ループ用インデックス

    // コマンドライン引数から点数を取得する。
    for (; i < pointCount; i++) {
      points[i] = Integer.parseInt(args[i]);
    }

    // 合計点を計算する。
    for (i = 0; i < pointCount; i++) {
      total += points[i];
    }

    // 平均点を計算する。
    average = (float) total / pointCount;

    // 平均点以上の人数を計算する。
    for (i = 0; i < pointCount; i++) {
      if (average <= points[i]) {
        overAveragePersonCount++;
      }
    }

    // 合計点を出力する。
    System.out.println("合計点は" + total + "です");

    // 平均点を出力する。
    System.out.println("平均点は" + average + "です");

    // 平均点以上の人数を表示する。
    System.out.println("平均点以上は" + overAveragePersonCount + "人です");
  }
}
