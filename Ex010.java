/*
 * Ex010.java
 *
 * Copyright(c) 2015 All Rights Reserved by Shinya Kinoshita
 */
import java.util.Scanner;

/**
 * キーボードからアンケートの選択肢(0～2)を入力し、集計とその結果を出力する。
 *
 * <p>
 * 入力を終え、集計する場合には、9を入力することとする。
 *
 * <p>
 * なお、0、1、2、9以外の数字が入力された場合は「0～2の数字を入力してください」
 * と表示し、再度、数字の入力を求めることとする。
 *
 * @author Shinya Kinoshita
 * @version 1.0
 * @since JDK 6.0
 */
public class Ex010 {

  /**
   * プログラムを起動する。
   *
   * @param args コマンドライン引数 (引数無し)
   * @throws NumberFormatException キーボード入力時に数字以外が入力された場合
   */
  public static void main(String[] args) {
    // 変数の宣言及び初期化をする。
    final int SELECTED_ITEM_SATISFIED = 0;    // アンケート選択肢：満足
    final int SELECTED_ITEM_NEITHER = 1;      // アンケート選択肢：どちらでもない
    final int SELECTED_ITEM_COMPLAINED = 2;   // アンケート選択肢：不満
    final int SELECTED_ITEM_FINISHED = 9;     // アンケート選択肢：終了

    int[] questionnaireResults = new int[3];  // アンケート集計結果
    int selectedNumber = 0;                   // 選択肢
    boolean questionaireContinued = true;     // アンケート継続状態
    int i = 0;                                // ループ用インデックス
    int j = 0;                                // ループ用インデックス

    Scanner scanner = new Scanner(System.in); // キーボードスキャナー

    // アンケートの選択肢を取得し、集計する。(研修の満足度)
    System.out.println("【質問】研修の満足度は？");
    System.out.println("【選択肢】0:満足 1:どちらでもない 2:不満");
    System.out.println("選択肢を入力してください");

    while (questionaireContinued) {
      selectedNumber = Integer.parseInt(scanner.nextLine());

      switch (selectedNumber) {
        case SELECTED_ITEM_SATISFIED:
          // 満足の場合
          questionnaireResults[SELECTED_ITEM_SATISFIED]++;
          break;
        case SELECTED_ITEM_NEITHER:
          // どちらでもないの場合
          questionnaireResults[SELECTED_ITEM_NEITHER]++;
          break;
        case SELECTED_ITEM_COMPLAINED:
          // 不満の場合
          questionnaireResults[SELECTED_ITEM_COMPLAINED]++;
          break;
        case SELECTED_ITEM_FINISHED:
          // 終了の場合
          System.out.println("入力を終了します");
          questionaireContinued = false;
          break;
        default:
          // 上記以外の場合
          System.out.println("0～2の数字を入力してください");
      }
    }

    // 集計結果を出力する。
    for (; i < questionnaireResults.length; i++) {
      System.out.print(i + ":");

      for (j = 0; j < questionnaireResults[i]; j++) {
        System.out.print("■");
      }

      System.out.println("(" + questionnaireResults[i] + ")");
    }
  }
}
