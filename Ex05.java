/*
 * Ex05.java
 *
 * Copyright(c) 2015 All Rights Reserved by Shinya Kinoshita
 */

/**
 * コマンドライン引数から２つの整数を取得し、小さい整数から大きい整数までの合計を出力する。
 *
 * @author Shinya Kinoshita
 * @version 1.0
 * @since JDK 6.0
 */
public class Ex05 {

  /**
   * プライベートコンストラクタ。
   */
  private Ex05() {
  }

  /**
   * プログラムを起動する。
   *
   * @param args コマンドライン引数 {@code args[0]} : 数値１,
   *     {@code args[1]} : 数値２
   * @throws ArrayIndexOutOfBoundsException {@code args.length < 2} の場合
   * @throws NumberFormatException {@code args[0]} もしくは
   *     {@code args[1]} が数字以外の場合
   */
  public static void main(String[] args) {
    // 変数の宣言及び初期化をする。
    int minNumber = 0;      // 小さい整数
    int maxNumber = 0;      // 大きい整数
    int tempNumber = 0;     // 一時保存用
    int total = 0;          // 小さい整数から大きい整数までの合計
    int i = 0;              // ループ用インデックス

    // コマンドライン引数から小さい整数及び大きい整数を取得する。
    minNumber = Integer.parseInt(args[0]);
    maxNumber = Integer.parseInt(args[1]);

    if (minNumber > maxNumber) {
      // 大きさが逆なので、小さい整数と大きい整数を入れ替える。
      tempNumber = minNumber;
      minNumber = maxNumber;
      maxNumber = tempNumber;
    }

    // 小さい整数から大きい整数までの合計を計算する。
    for (i = minNumber; i <= maxNumber; i++) {
      total += i;
    }

    // 上記の計算結果を出力する。
    System.out.println(
        minNumber + "から" + maxNumber + "まで合計は" + total + "です");
  }
}
