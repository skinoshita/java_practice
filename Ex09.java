/*
 * Ex09.java
 *
 * Copyright(c) 2015 All Rights Reserved by Shinya Kinoshita
 */
import java.util.Scanner;

/**
 * キーボードから名前を入力し、「ようこそ○○さん」を表示する。
 *
 * @author Shinya Kinoshita
 * @version 1.0
 * @since JDK 6.0
 */
public class Ex09 {

  /**
   * プログラムを起動する。
   *
   * @param args コマンドライン引数 (引数無し)
   */
  public static void main(String[] args) {
    // 変数の宣言及び初期化をする。
    String name = "";                           // 名前
    String message = "";                        // 表示メッセージ
    Scanner scanner = new Scanner(System.in);   // キーボードスキャナー

    // キーボード入力から名前を取得する。
    while(true) {
      System.out.print("名前>");
      name = scanner.nextLine();

      if (name.length() > 0) {
        // 名前が入力された場合
        break;
      }

      System.out.println("名前は必ず入力してください");
    }

    // 「ようこそ○○さん」の形式で名前を表示する。
    System.out.println("ようこそ" + name + "さん");
  }
}
