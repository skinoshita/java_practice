/*
 * Ex02.java
 *
 * Copyright(c) 2015 All Rights Reserved by Shinya Kinoshita
 */

/**
 * １日目１０円、２日目２０円のように、毎日貯金していき、コマンドライン引数から入力された
 * 金額以上溜まった時点の日数を出力する。
 *
 * @author Shinya Kinoshita
 * @version 1.0
 * @since JDK 6.0
 */
public class Ex02 {

  /**
   * プライベートコンストラクタ。
   */
  private Ex02() {
  }

  /**
   * プログラムを起動する。
   *
   * @param args コマンドライン引数 {@code args[0]} : 貯金目標金額
   * @throws ArrayIndexOutOfBoundsException {@code args.length == 0} の場合
   * @throws NumberFormatException {@code args[0]} が数字以外の場合
   */
  public static void main(String[] args) {
    // 変数の宣言及び初期化をする。
    int targetStockPrice = 0;             // 貯金目標額
    int stockCompleteDays = 0;            // 貯金目標達成日数
    int stockPricePerDay = 10;            // 毎日の貯金額
    int totalStockPrice = 0;              // 合計貯金額
    final int INCREMENT_STOCK_PRICE = 10; // １日毎に増加する貯金額

    // 貯金目標額をコマンドラインから取得する。
    targetStockPrice = Integer.parseInt(args[0]);

    // 貯金目標額に到達する日数を計算する。
    while (totalStockPrice < targetStockPrice) {
      totalStockPrice += stockPricePerDay;
      stockPricePerDay += INCREMENT_STOCK_PRICE;
      stockCompleteDays++;
    }

    // 貯金目標額に到達する日数を出力する。
    System.out.println(
        targetStockPrice + "円は" + stockCompleteDays + "日で貯まります");
  }
}
